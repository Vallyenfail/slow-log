package run

import (
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	db2 "gitlab.com/Vallyenfail/slow-log/db"
	"gitlab.com/Vallyenfail/slow-log/geo"
	cservice "gitlab.com/Vallyenfail/slow-log/module/courier/service"
	cstorage "gitlab.com/Vallyenfail/slow-log/module/courier/storage"
	"gitlab.com/Vallyenfail/slow-log/module/courierfacade/controller"
	cfservice "gitlab.com/Vallyenfail/slow-log/module/courierfacade/service"
	oservice "gitlab.com/Vallyenfail/slow-log/module/order/service"
	ostorage "gitlab.com/Vallyenfail/slow-log/module/order/storage"
	"gitlab.com/Vallyenfail/slow-log/router"
	"gitlab.com/Vallyenfail/slow-log/server"
	"gitlab.com/Vallyenfail/slow-log/workers/order"
	"net/http"
	"os"
)

type App struct {
}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() error {
	// получение хоста и порта redis
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPwd := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")

	// инициализация клиента redis
	db, err := db2.NewPostgresDB(dbHost, dbPort, dbUser, dbPwd, dbName)
	if err != nil {
		return err
	}

	// инициализация разрешенной зоны
	allowedZone := geo.NewAllowedZone()
	// инициализация запрещенных зон
	disAllowedZones := []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}

	// инициализация хранилища заказов
	orderStorage := ostorage.NewOrderStorage(db)
	// инициализация сервиса заказов
	orderService := oservice.NewOrderService(orderStorage, allowedZone, disAllowedZones)

	orderGenerator := order.NewOrderGenerator(orderService)
	orderGenerator.Run()

	oldOrderCleaner := order.NewOrderCleaner(orderService)
	oldOrderCleaner.Run()

	// инициализация хранилища курьеров
	courierStorage := cstorage.NewCourierStorage(db)
	// инициализация сервиса курьеров
	courierService := cservice.NewCourierService(courierStorage, allowedZone, disAllowedZones)

	// инициализация фасада сервиса курьеров
	courierFacade := cfservice.NewCourierFacade(courierService, orderService)

	// инициализация контроллера курьеров
	courierController := controller.NewCourierController(courierFacade)

	// инициализация роутера
	routes := router.NewRouter(courierController)
	// инициализация сервера
	r := server.NewHTTPServer()
	// инициализация группы роутов
	api := r.Group("/api")
	// инициализация роутов
	routes.CourierAPI(api)

	mainRoute := r.Group("/")

	routes.Swagger(mainRoute)
	routes.Prometheus(mainRoute)
	prometheus.MustRegister(controller.CourierGetStatus, controller.MoveCourier, ostorage.RepoStatus)
	// инициализация статических файлов
	r.NoRoute(gin.WrapH(http.FileServer(http.Dir("public"))))

	// запуск сервера

	if os.Getenv("ENV") == "prod" {
		certFile := "/app/certs/cert.pem"
		keyFile := "/app/certs/private.pem"
		return r.RunTLS(":443", certFile, keyFile)
	}

	return r.Run()
}
