package models

import (
	cm "gitlab.com/Vallyenfail/slow-log/module/courier/models"
	om "gitlab.com/Vallyenfail/slow-log/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
